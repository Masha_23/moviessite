import React from 'react';
import logo from './logo.svg';
import './App.css';
import {BrowserRouter, Route} from 'react-router-dom';

import HomePage from './pages/homePage';
import MoviePage from './pages/moviePage';
import Layout from './components/Layout';

function App() {
  return (
      <BrowserRouter>
          <Layout>
              <div className={'container'}>
                  <Route path={'/home'}>
                      <HomePage title={'home'}/>
                  </Route>
                  <Route path={'/popular'}>
                      <HomePage title={'popular'}/>
                  </Route>
                  <Route path={'/upcoming'}>
                      <HomePage title={'upcoming'}/>
                  </Route>
                  <Route path={'/toprated'}>
                      <HomePage title={'toprated'}/>
                  </Route>
                  <Route path={'/movie/:id'}>
                      <MoviePage title={'toprated'}/>
                  </Route>
              </div>
          </Layout>
      </BrowserRouter>
  );
}

export default App;
