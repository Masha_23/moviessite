import React from 'react';
import Api from '../api';
import { Row, Col, Divider } from 'antd';
import Card from '../components/Card';
const style = { background: '#0092ff', padding: '8px 0'};

let pages = {
    'home' : 'Home Page',
    'popular' : 'Popuplar',
    'upcoming' : 'Upcoming',
    'toprated' : 'Top Rated'
};

function HomePage(props) {
    const [movies, setMovies] = React.useState([]);
    const {title} = props;

    React.useEffect(() => {
        Api.getMovies(title).then(res => {
            setMovies(res.results);
        });
    }, []);

    return (
    <>
        <h1>{title}</h1>
        <Row gutter={16}>
            {movies.map(movie => (
                <Col className="gutter-row" sm={2} lg={6} md={6}>
                    <Card
                        id={movie.id}
                        title={movie.original_title}
                        src={'https://image.tmdb.org/t/p/w500//'+movie.poster_path}
                        description={movie.overview}
                    />
                </Col>
            ))}
        </Row>
    </>
    );
}

export default HomePage;

