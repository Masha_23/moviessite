import React from 'react';
import {useParams} from 'react-router-dom';
import Api from '../api';

const MoviePage = () => {
    const {id} = useParams();
    const [movie, setMovie]   = React.useState({});

    React.useEffect( () => {
        Api.getMovieById(id).then(movie => setMovie(movie))
    }, []);

    return (
        <div>
            {movie.title}
        </div>
    );
};

export default MoviePage;