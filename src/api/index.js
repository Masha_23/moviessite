const BASE_URL = "https://api.themoviedb.org";
const API_KEY = "3405a79ceea0339273262cb2d094ec3b";

export default class Api {
    static getMovieById(id) {
        return fetch(`${BASE_URL}/3/movie/${id}?api_key=${API_KEY}`).then(res => res.json());
    }

    static getMovies(page) {
        let url = `${BASE_URL}/3/discover/movie?api_key=${API_KEY}`;
        if (page === 'toprated') {
            url = `${BASE_URL}/3/movie/top_rated?api_key=${API_KEY}`;
        }
        else if (page === 'popular') {
            url = `${BASE_URL}/3/movie/popular?api_key=${API_KEY}`;
        }
        else if (page === 'upcoming') {
            url = `${BASE_URL}/3/movie/upcoming?api_key=${API_KEY}`;
        }

        return fetch(url).then(res => res.json());
    }
}





