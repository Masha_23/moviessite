import React from 'react';
import 'antd/dist/antd.css';
import { Layout } from 'antd';
import Menu from '../Menu';
const { Header, Footer, Sider, Content } = Layout;

const CustomLayout = (props) => {
    const [collapsed, setCollapsed] = React.useState(false);
    const onCollapse = () => {
        setCollapsed(!collapsed);
    }

    const {children} = props;

    return (
    <Layout style={{ minHeight: '100vh' }}>
        <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
            <Menu/>
        </Sider>
        <Layout className="site-layout">
            <Header className="site-layout-background" style={{ padding: 0 }} />
            <Content style={{ margin: '0 16px' }}>
                {children}
            </Content>
            <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
        </Layout>
    </Layout>
    );
}



export default CustomLayout;
