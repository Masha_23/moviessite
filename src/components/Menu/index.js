import React from 'react';
import {Link} from "react-router-dom";
import 'antd/dist/antd.css';
import { Menu, Switch, Divider } from 'antd';
import {
    HomeOutlined,
    StarOutlined,
    ThunderboltOutlined,
    RocketOutlined,
    MenuUnfoldOutlined
} from '@ant-design/icons';

const { SubMenu } = Menu;

class Sider extends React.Component {
    render() {
        return (
            <>
                <Menu
                    defaultSelectedKeys={['1']}
                    defaultOpenKeys={['sub1']}
                >
                    <Menu.Item key="1" icon={<HomeOutlined />}>
                        <Link to="/home">
                            Home
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="2" icon={<ThunderboltOutlined />}>
                        <Link to="/popular">
                            Popular
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="3" icon={<RocketOutlined />}>
                        <Link to="/upcoming">
                            Upcoming
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="4" icon={<StarOutlined />}>
                        <Link to="/toprated">
                            Top Rated
                        </Link>
                    </Menu.Item>
                </Menu>
            </>
        );
    }
}

export default Sider;