import React from 'react';
import 'antd/dist/antd.css';
import { Card } from 'antd';
import {Link} from "react-router-dom";

const { Meta } = Card;

const CustomCard = (props) => {
    return (
        <Link to={`/movie/${props.id}`}>
            <Card
                hoverable
                style={{margin: '0 0 20px 0'}}
                cover={<img alt="example" src={props.src} />}
            >
                <Meta title={props.title} description={props.description} />
            </Card>
        </Link>
    );
}

export default CustomCard;
